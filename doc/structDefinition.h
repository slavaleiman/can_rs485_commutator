#pragma once
#include "types.h"

//Устанавливем вырванивание в 1 байт для всех последующих структур
#pragma pack(push, 1)

#define ST_M_CurParametrs_ID        0
#define ST_M_TrainParam_ID          1
#define ST_M_Profil_ID              2
#define ST_M_SpeedLimit_ID          3
#define ST_M_BlokSection_ID         4
#define ST_M_Schedule_ID            5
#define ST_M_CAN_Message_ID         6

#define ST_AV_Commands_ID           0
#define ST_AV_CurrentParams_ID      1
#define ST_AV_PredictSpeed_ID       2
#define ST_AV_CAN_Message_ID        3
#define ST_AV_Firmware_Version_ID   4
#define ST_AC_MSUL_CurParam_ID      5


/*
 * Заговолок UDP-пакета
 * 8 байт
*/
struct AV_PACKAGE_HEADER {
    byte            message_type;    //тип сообщения
    ulong           package_number;  //номер пакета
    WORD            crc16_csum;      //контрольная сумма пакета
    byte            reserve;         //резервный байт, для отладочной информации, etc
};


/*
 * Текущие параметры
 * 31 байт
*/
struct ST_M_CurParametrs
{
    DateTime		Date_Time;			//Дата и Время
    LongLength		PathGen;			//расстояние, пройденное от (до) выходного генератора текущего перегона, м
    Speed			MySpeed;			//фактическая скорость движения, км/ч
    Speed			Vpr_BLOK;			// Допустимая скорость от систем безопасности (1 бит = 0.01 км/ч)
    byte			flag_EmBrake;		// Признак о применении экстренного торможения  (УКТОЛ – 6 положение ККМ),  в том числе срыв ЭПК от БЛОК
    ByteBool		isBrokenTM;			//Сигнал датчика «Обрыв ТМ» ведущей секции
    byte			SignalAutoblock;	//Сигналы автоблокировки / свободные блок участки+
                                        //1 Сигналы автоблокировки/свободные блок участки/РЦ:
                                        //  0 - белый
                                        //  1 - красный по АЛСН
                                        //  2 - желтый с красным по АЛСН
                                        //  3 - желтый
                                        //  4 - зеленый
                                        //  5-7 - погашен
                                        //  8 - белый мигающий
                                        //  9 - красный по АЛС-ЕН
                                        //  10 - желтый с красным по АЛС-ЕН
                                        //  11 - 0 свободная РЦ
                                        //  12 - 1 свободных РЦ
                                        //  13 - 2 свободных РЦ
                                        //  14 - 3 свободных РЦ
                                        //  15 - 4 свободных РЦ
                                        //  16 - 5 свободных РЦ
                                        //  17 - 6 свободных РЦ
                                        //  18 - 7 свободных РЦ
                                        //  19 - 8 свободных РЦ
                                        //  20 - 9 свободных РЦ
                                        //  21 - 10 свободных РЦ
                                        //  22..255 – не используется
    ByteBool		isAutoblock;		//Признак наличия автоблокировки (да/нет)
    byte			WorkMode;			//Выбранный режим движения: 0 – не выбран; 1 – советчик; 2 – АВ.
    byte			ScheduleMode;		//Режим исполнения расписания: 0 – не выбран; 1 – по расписанию; 2 – по удалению; 3…9 – нагон (1...7); 10 – синхронно.
    Single			PressureTM;			//Давление ТМ, МПа
    Single			PressureUR;			//Давление УР, МПа
    Single			PressureTC;			//Давление ТЦ, МПа
    byte			isPneumoBrake;		//1 = 63 Применение ПТ	0 – нет;	1 – СТ1;	2 – СТ2;	3 – ОТП.
};

/*
 * Параметры автомотрисы
 * 19 байт
*/
struct ST_M_TrainParam
{
    ShortLength	Length;				    //Длина поезда, м
    WORD			TrainWeight;		//Масса поезда, т
    Speed			SpeedYellow;		//Скорость проследования «Ж» огня (1 бит = 0.01 км/ч)
    Single			DischargeYPIStage;	//Разрядка УР I ступень торможения, МПа (min 0,03 - max 0,15) [по умолчанию 0,06]
    Single			DischargeYPIIStage;	//Разрядка УР II ступень торможения, МПа (min 0,03 - max разрядка УР II + разрядка УР I ? 0,15) [по умолчанию 0,04]
    Single          zTM;	            //Завышение ТМ, МПа max 0,15 [по умолчанию 0,05]
    byte v_o;                           //Дополнительное Vогр. (20…80) км/ч
};

/*
 * Запись массива профиля пути
 * 6 байт
*/
struct	ST_M_Profil
{
    LongLength		S;					//расстояние до точки, с которой начинается элемент профиля, м
    Profile		    I;					//уклон (1/1000)
};

/*
 * Запись массива разрешенных скоростей движения
 * 6 байт
*/
struct ST_M_SpeedLimit
{
    LongLength		S;					//расстояние до точки, с которой начинается ограничение, м
    Speed			V;					//скорость, км/ч
};

/*
 * Запись массива блок-участков
 * 5 байт
*/
struct ST_M_BlokSection
{
    LongLength		S;					//расстояние до точки, на которой находится граница блок-участка, м
    byte			N;					//N - Номер светофора: 0 – входной, маршрутный; 1,3,5... – нечетные проходные; 2,4,6… – четные проходные; Выходной на 2 больше максимального номера проходного.
};

/*
 * Запись массива расписания движения
 * 12 байт
*/
struct ST_M_Schedule
{
    LongLength		S;					//расстояние до остановки головного вагона, м
    DateTime		T1;					//время прибытия/проследования
    DateTime		T2;					//время отправления
};

/*
 * Запись массива сообщений из/в CAN
 * 11 байт
*/
struct ST_CAN_Message
{
    WORD            id;				    //Идентификатор
    byte            data_len;		    //Длина сообщения
    byte            data[8];		    //Данные
};

/*
 * Команды от автоведения
 * 3 байта
*/
struct ST_AV_Commands
{
    SHORT			TractionBrakingPow;	//Заданная/рекомендуемая сила тяги / торможения, %
    ByteBool		isABReady;			//Готовность автоведения к работе (да/нет)
};

/*
 * Текущие параметры для отображения
 * 14 байт
*/
struct ST_AV_CurrentParams
{
    Speed			PredictSpeed;		//Расчетная скорость, км/ч
    AV_Time	        DevFromSchedule;	//Отклонение от графика движения в секундах
    DateTime		TimeCurStation;		//Время прибытия/проследования впередилежащей станции
    DateTime		TimeNextStation;	//Время прибытия на станцию следующую за впередилежащей
};

/*
 * Запись массива для отображения расчетной (прогнозной) скорости движения
 * 6 байт
*/
struct ST_AV_PredictSpeed
{
    ShortLength	S;					    //координата точки (расстояние от головы поезда, м)
    Speed			V;					//скорость в точке(1 бит = 0.01 км/ч)
};

/*
 * Дополнительная информация
 * 4 байта
*/
struct ST_AV_Firmware_Version {
    int             version;            //Версия
};

/*
 * Текущие параметры
 * 8 байт
*/
struct ST_AV_MSUL_CurParam
{
    byte			PowerMode;		    //Режим силовой схемы: 0 – Выбег, 1 – Тяга, 2 – Рекуперативное торможение, 3 - Реостатное торможение
    SHORT			MaxArmoryCur;       //Максимальный фактический ток якоря
    ByteBool		isAV_ON;		    //Включено управление АВ (да/нет)
    ByteBool		isBlockAV;			//Блокировка режима АВ (да/нет)
    ByteBool		isMoveReady;		//Готовность автоморисы к движению
    Force			FactForce;			//Фактическая сила тяги / торможения:  по формуле: F(Byte) = 101 + F(Short), где F(Short) = -101..154.
    byte			PositionTED;		//Номер позиции включения ТЭД (только для локомотивов с контакторным регулированием)
};

//Конец выравнивания
#pragma pack(pop)
