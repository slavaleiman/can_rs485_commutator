#ifndef TYPES_H
#define TYPES_H

typedef unsigned char byte;
typedef unsigned short WORD;
typedef unsigned int LongWord;
typedef byte ByteBool;
typedef short SHORT;
typedef float Single;
typedef unsigned long DateTime;	//32 bit unsigned int время в формате UNIX (количество истекших секунд с  00:00:00 01.01.1970)
typedef float LongLength;
typedef WORD ShortLength;
typedef WORD Speed;                 //1 ед. = 0.01 км/ч,
typedef SHORT Profile;				//1 ед. = 0.01
typedef byte Force;                 //по формуле: F(TForce) = 101 + F(Short), где F(Short) = -101…154.
typedef int AV_Time;				//32-bit signed integer (количество секунд)
typedef byte MPSUiD_AV_Force;

typedef unsigned int uint;
typedef unsigned long ulong;

#endif // TYPES_H
