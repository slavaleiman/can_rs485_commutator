#include "unity.h"
#include "modbus_port.h"
#include <string.h>
#include <stdio.h>

void setUp(void)
{
}

void tearDown(void)
{
}

modbus_port_t mbport = {0};

void test_modbus_port(void)
{
	uint16_t test_regs_count = 128;

	for(uint8_t repeats = 0; repeats < 4; ++ repeats)
	{
		for(uint16_t reg = 0; reg < test_regs_count; ++reg)
		{
			uint16_t value = repeats + reg;
			if(mbport_write_data(&mbport, reg, value))
				printf("WRITE ERROR\n");
		}
	}
	TEST_ASSERT_EQUAL(test_regs_count, mbport_regs_count(&mbport));

	uint8_t reg_offset = 6;
	test_regs_count = 129;
	for(uint8_t repeats = 0; repeats < 4; ++ repeats)
	{
		for(uint16_t reg = reg_offset; reg < test_regs_count + reg_offset; ++reg)
		{
			uint16_t value = repeats + reg;
			if(mbport_write_data(&mbport, reg, value))
				printf("WRITE ERROR\n");
		}
	}
	printf("regs count:%d\n", mbport_regs_count(&mbport));
	TEST_ASSERT_NOT_EQUAL(test_regs_count, mbport_regs_count(&mbport));
}

int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_modbus_port);
    return UNITY_END();
}
