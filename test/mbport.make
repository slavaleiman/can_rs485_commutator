# ==========================================
#   Unity Project - A Test Framework for C
#   Copyright (c) 2007 Mike Karlesky, Mark VanderVoord, Greg Williams
#   [Released under MIT License. Please refer to license.txt for details]
# ==========================================

#We try to detect the OS we are running on, and adjust commands as needed
ifeq ($(OS),Windows_NT)
  ifeq ($(shell uname -s),) # not in a bash-like shell
	CLEANUP = del /F /Q
	MKDIR = mkdir
  else # in a bash-like shell, like msys
	CLEANUP = rm -f
	MKDIR = mkdir -p
  endif
	TARGET_EXTENSION=.exe
else
	CLEANUP = rm -f
	MKDIR = mkdir -p
	TARGET_EXTENSION=
endif

C_COMPILER=gcc
ifeq ($(shell uname -s), Darwin)
C_COMPILER=clang
endif

UNITY_ROOT=../../Unity

CFLAGS = -std=c99
CFLAGS += -Wall
CFLAGS += -Wattributes
CFLAGS += -Wextra
CFLAGS += -Wpointer-arith
CFLAGS += -Wcast-align
CFLAGS += -Wwrite-strings
CFLAGS += -Wswitch-default
CFLAGS += -Wunreachable-code
CFLAGS += -Winit-self
CFLAGS += -Wmissing-field-initializers
CFLAGS += -Wno-unknown-pragmas
CFLAGS += -Wstrict-prototypes
CFLAGS += -Wundef
CFLAGS += -Wold-style-definition 
CFLAGS += -g3
CFLAGS += -m32

TARGET=test_mbport

SRC_FILES=$(UNITY_ROOT)/src/unity.c ../Src/modbus_port.c  test_modbus_port.c 

INC_DIRS=-I../Src \
 -I$(UNITY_ROOT)/src \
 -I../Src \
 -I../.	\
 -DUNITTEST=1	\
 -DSTM32F407xx	\
 -I../Src/	\
#  -I../libs/CMSIS/Include -I../libs/CMSIS/Device/ST/STM32F4xx/Include	\

SYMBOLS=

all: mbport

mbport: $(SRC_FILES)
	$(C_COMPILER) $(CFLAGS) $(INC_DIRS) $(SYMBOLS) $(SRC_FILES) -o $(TARGET)
	./$(TARGET)
clean:
	$(CLEANUP) $(TARGET)
