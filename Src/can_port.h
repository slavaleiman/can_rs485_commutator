#ifndef __CAN_PORT__
#define __CAN_PORT__ 1

#include "stm32f4xx.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"
#include "cmsis_os.h"
#include "circular_buffer.h"

// PDU1 - destinated message
// PDU2 - broadcast message
// example ID - 18 FF B5 F2
// 				prio + reserve + data page
//				   PF - PDU format - addressed or broadcast
//					  PS - PDU specific - based on PF, if (PF < 240) PS contains dest addr
//					  								   if (PF > 240) PS contains group extension
//						 source address - who send this message, 254 available

// TODO настроить фильтр FIFO1 на броадкаст
					  // FIFO2 на ошибки
typedef struct{
	uint32_t 	ts; // timestamp millisec
	uint32_t 	id;
	uint8_t 	data[8];
	uint16_t 	len;
}__attribute__((packed)) j1939_message_t;

#define CAN_DATA_BUFFER_LENGTH 256
#define CAN_ERROR_BUFFER_LENGTH 32

typedef struct 
{
	CAN_HandleTypeDef* 	hcan;
    QueueHandle_t 		RxQueue;
	osSemaphoreId 		SemaphoreHandle;
	uint32_t 			baudrate;
	j1939_message_t 	events[CAN_DATA_BUFFER_LENGTH];
	// если обнаружено сообщение с ошибкой, оно попадает сюда и сохраняется в ROM памяти
	j1939_message_t 	errors[CAN_ERROR_BUFFER_LENGTH]; 
	circular_buffer_t* 	cbuffer;
}can_port_t;

void can_port_process(can_port_t* can_port);

void can_port_init(can_port_t* 			can_port,
					CAN_HandleTypeDef* 	hcan,
				    QueueHandle_t 		RxQueue,
					osSemaphoreId 		SemaphoreHandle,
					circular_buffer_t* 	cbuffer
					);
void can_port_on_read(can_port_t* can_port, uint8_t* msg, size_t len);

// uint8_t* can_port_data(can_port_t* can_port);

// void can_port_process_output(uint32_t PGN, uint32_t SPN, uint8_t* data); // for put to modbus

#endif
