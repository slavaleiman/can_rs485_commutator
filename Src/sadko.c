#include "sadko.h"

// для отправки параметров данных без хранения всех параметров
// из модулей diesel.c и skrt.c используется sadko_send_spn
// так же по протоколу садко посылаются данные модуля управления
// некоторые spn соответствую данным из протокола

#define SADKO_BUFFER_SIZE 255

typedef struct
{
	bool is_change_flag;
	uint8_t tx_buffer[SADKO_BUFFER_SIZE];
}sadko_t;

sadko_t sadko;
bool sadko_is_changes(void)
{
	return sadko.is_change_flag;
}

// длина данных приходит в сообщении 
// а в модбас кладется в определенном формате (каком?)

void sadko_send_spn(uint16_t spn, uint8_t* data, uint8_t len)
{
	
}

int8_t sadko_on_rtu(modbus_port_t* mbport, uint8_t* message, uint16_t size)
{
    const uint8_t addr = message[0];
    // mbproxy_inc_total_rq();

    // if(size < 8)
        // return -1;

    if(addr == SADKO_ADDR)
    {
        const uint8_t func_num = message[1];
        if(func_num & 0x80)
        	return -1;
        // switch(func_num)
        // {
            // case 3: // чтение значений из нескольких регистров хранения (Read Holding Registers).
            //     return modbus_f3(mbport, message);
            // case 6: // запись значения в один регистр хранения
            //     return modbus_f6(mbport, message);
            // case 8: // Диагностика
            //     return modbus_response_echo(mbport, message);
            // case 16: // запись значений в несколько регистров хранения (Preset Multiple Registers)
            //     return modbus_f16(mbport, message, size);
            // default:
            //     modbus_response_error(mbport, message, FUNC_CODE_ERR);
        // }
    }else
        return -1;
    return 0;
}

void sadko_process_input(modbus_port_t* mbport)
{
	osSemaphoreId UsartRxSemaphoreHandle = mbport->UsartRxSemaphoreHandle;
	QueueHandle_t RxQueue = mbport->RxQueue;
	if(xSemaphoreTake(UsartRxSemaphoreHandle, pdMS_TO_TICKS(15)) == pdTRUE)
	{
		message_t mess;
		if(RxQueue)
		{
			if(xQueueReceive(RxQueue, &mess, pdMS_TO_TICKS(15)))
			{
				if(mess.len != 0)
				{
					int8_t ret;
					ret = sadko_on_rtu(mbport, mess.data, mess.len);
					if(ret)
					{
						errors_on_error(SADKO_BAD_RETURN);
					}
					memset(mess.data, 0, mess.len);
				}
			}
		}
	}
}

void sadko_process_output(modbus_port_t* mbport)
{
	// вот тут в зависимости от того какой сконфигурирован интерфейс связи с садко
#ifdef SADKO_OUTPUT_RS458
	
#elif SADKO_OUTPUT_CAN
	
#endif
}
