#ifndef __SADKO__
#define __SADKO__ 1
#include "modbus.h"
#include "modbus_port.h"
#include "dma_usart_idle.h"
#include "errors.h"

void sadko_process_input(modbus_port_t* mbport);
void sadko_process_output(modbus_port_t* mbport);
bool sadko_is_changes(void);
#define SADKO_ADDR 	31

// #define SADKO_OUTPUT_RS458 1
#define SADKO_OUTPUT_CANBUS 1

#endif
