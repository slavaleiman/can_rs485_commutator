/*
    this module is can filter with 2 buffers 
    здесь должен быть фильтр из которого можно взять значения параметров дизеля и СКТ
    и отправить в ЭСУТ по modbus как слейв
*/
#include "can_port.h"
#include "circular_buffer.h"

can_port_t can_port;
// при добавлении пакета, нужно пробегать по всем ID в текущем списке
// добавление происходит без сортировки массив статический
// сравнивается ID пакетов

// если закончилось место в массиве - перезаписывать по приоритету
    // найти пакет с меньшим приоритетом и перезаписать

void can_port_init(can_port_t* can_port,
                    CAN_HandleTypeDef* hcan,
                    QueueHandle_t RxQueue,
                    osSemaphoreId SemaphoreHandle,
                    circular_buffer_t* cbuffer
                    )
{
    can_port->hcan = hcan;
    can_port->RxQueue = RxQueue;
    can_port->SemaphoreHandle = SemaphoreHandle;
}

extern CAN_HandleTypeDef hcan1;
extern CAN_HandleTypeDef hcan2;

extern circular_buffer_t can1_cbuffer;
extern circular_buffer_t can2_cbuffer;

void can_port_process(can_port_t* can_port)
{
    osSemaphoreId SemaphoreHandle   = can_port->SemaphoreHandle;
    QueueHandle_t RxQueue           = can_port->RxQueue;
    if(xSemaphoreTake(SemaphoreHandle, pdMS_TO_TICKS(15)) == pdTRUE)
    {
        j1939_message_t mess;
        uint8_t output[16];
        if(RxQueue)
        {
            if(xQueueReceive(RxQueue, &mess, pdMS_TO_TICKS(15)))
            {
                if(mess.len != 0)
                {
                    cbuffer_pop(can_port->cbuffer, (uint8_t*)output, mess.len);
                    can_port_on_read(can_port, output, mess.len);
                }
            }
        }
    }
}

void can_port_process_output(void)
{
    // get from train control panel
    // send to CAN_output - to sadko
}

int16_t can_port_get_index(can_port_t* can_port, uint8_t* msg)
{
    uint32_t input_id = (msg[0] << 24) | (msg[1] << 16) | (msg[2] << 8) | msg[3];
    for(uint8_t i = 0; i < CAN_DATA_BUFFER_LENGTH; ++i)
    {
        uint32_t can_id = can_port->events[i].id;
        if(can_id == input_id)
            return i;
    }
    return -1;
}

int16_t can_port_table_update_data(can_port_t* can_port, uint16_t index, uint8_t* msg)
{
    uint8_t* data = &msg[4];
    for(uint8_t i = 0; i < 8; ++i)
    {
        can_port->events[index].data[i] = data[i];
    }
    return 0;
}

int16_t can_port_table_insert_new_item(can_port_t* can_port, uint8_t* msg)
{
    return 0;
}

static inline uint32_t get_pgn(uint32_t id)
{
    /* 18-bit parameter group number */
    return (uint32_t) ((id >> 8U) & ((1U << 18U) - 1)); // ?
}

uint16_t can_port_get_msg_index(can_port_t* can_port, uint8_t* msg)
{
    // TODO search PGN in buffer and return index or -1
    return -1;
}

void can_port_table_push(can_port_t* can_port, uint8_t* msg)
{
    // find id if is_exist

    // в ID лежит PGN 
    // а в данных лежит SPN + значение
    // тоесть могут прийти сообщения с одним ID и разными SPN, которые надо сохранять отдельно?

    // uint32_t ID = (msg[0] << 24) | (msg[1] << 16) | (msg[2] << 8) | msg[3];

    // uint32_t pgn = get_pgn(ID); 

    // int16_t index = can_port_get_msg_index(can_port, msg);
    // if(index > 0)
    // {
    //     can_port_table_update_data(can_port, index, msg);
    // }
    // else
    // {
    //     can_port_table_insert_new_item(can_port, msg);
    // }

    // int16_t err_index = can_port_get_err_index(can_port, msg);
    // if(err_index)
    // {
    //     // add new packet
    // }
}

void can_port_on_read(can_port_t* can_port, uint8_t* msg, size_t len)
{
// example ID - 18 FF B5 F2
//              prio + reserve + data page
//                 PF - PDU format - addressed or broadcast
//                    PS - PDU specific - based on PF, if (PF < 240) PS contains dest addr
//                                                     if (PF > 240) PS contains group extension
//                       source address - who send this message, 254 available
/*    uint8_t DLC = msg[4];
    uint8_t PF = msg[1];
    uint8_t source_addr = msg[3];

    if(PF < 240) // adressed request, PS contains address
    {
        uint8_t dest_addr = msg[2];
        // NOT SAVED IN TABLE ??

    }else{ // broadcast, PS contains group extension
        uint8_t group_ext = msg[2];


        // search for packet in buffer and 
        // if(PS )
        {
            can_port_table_push(can_port, msg);
        }
    }*/
}

j1939_message_t* can_port_data(can_port_t* can_port)
{
    return can_port->events;
}

j1939_message_t* can_port_errors(can_port_t* can_port)
{
    return can_port->errors;
}
