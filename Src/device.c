// эта сущность содержит регистры с данными девайсов
// которыми прикидывается коммутатор

// данные хранятся в 16 битных регистрах
#include <stdbool.h>
#include "device.h"
#include "modbus.h"
#include "modbus_proxy.h"
#include "CRC.h"
#include "stm32f4xx.h"

bool find_reg(device_t* dev, uint16_t reg, uint16_t* index)
{
	if(!dev->regs_count)
		return false;
	for(uint8_t i = 0; i < DEVICE_NUM_REGS; ++i)
	{
		if(dev->data[i][0] == reg)
		{
			*index = i;
			return true;
		}
	}
	return false;
}

int8_t get_free_index(device_t* dev, uint16_t* index)
{
	if(!dev->regs_count)
	{
		*index = 0;
		return 0;
	}
	for(uint8_t i = 1; i < DEVICE_NUM_REGS; ++i)
	{
		if(dev->data[i][0] == 0)
		{
			*index = i;
			return 0;
		}
	}
	return -1;
}

int8_t device_read_data(device_t* dev, uint16_t reg, uint16_t* value)
{
	uint16_t index = 0;
	if(find_reg(dev, reg, &index))
	{
		*value = dev->data[index][1];
		return 0;
	}
	*value = 0;
	return -1;
}

void init_reg(device_t* dev, uint8_t index, uint16_t reg)
{
	printf("%s %d idx:%d\n", __FUNCTION__, reg, index);
	dev->data[index][0] = reg;
	++dev->regs_count;
}										

int8_t device_write_data(device_t* dev, uint16_t reg, uint16_t value)
{
	uint16_t index = 0;
	if(!find_reg(dev, reg, &index))
	{
		printf("reg not found %d\n", reg);
		int8_t ret = get_free_index(dev, &index);
		if(!ret)
		{
			if(index < DEVICE_NUM_REGS)
				init_reg(dev, index, reg);
			else
				return DATA_ADDR_ERR;
		}else{
			return DATA_ADDR_ERR;
		}
	}

	dev->data[index][1] = value;
	return 0;
}

uint16_t device_regs_count(device_t* dev)
{
	return dev->regs_count;
}
