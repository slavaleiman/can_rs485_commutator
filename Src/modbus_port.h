#ifndef __MODBUS_PORT__
#define __MODBUS_PORT__ 1

#include <string.h>
#include <stdbool.h>
#include "circular_buffer.h"
#include "device.h"
#include "soft_timer.h"
#ifndef UNITTEST
#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"
#include "cmsis_os.h"
#include "stm32f4xx.h"
#endif
#define DMA_RX_BUFFER_SIZE          255
#define PORT_BUFFER_SIZE           	255 // uint8_t

typedef struct
{
#ifndef	UNITTEST
	UART_HandleTypeDef *huart;
	IRQn_Type 			IRQn;
    uint32_t 			baudrate;
    QueueHandle_t 		RxQueue;
	osSemaphoreId 		UsartRxSemaphoreHandle;
    SemaphoreHandle_t 	holding_mutex;
#endif
    bool 				is_sending;
	uint8_t 			DMA_RX_Buffer[DMA_RX_BUFFER_SIZE];
	circular_buffer_t* 	cbuffer;

    uint8_t     		rx_buffer[PORT_BUFFER_SIZE];
    uint8_t     		rx_size;
    uint8_t     		tx_buffer[PORT_BUFFER_SIZE];
    uint8_t     		tx_size;

    soft_timer_t 		timer;
    uint32_t 			timeout;
    device_t* 			device;
}modbus_port_t;

void mbport_process(modbus_port_t* mbport);
#ifndef UNITTEST
void mbport_init(modbus_port_t* mbport,
					UART_HandleTypeDef *huart,
					IRQn_Type 	IRQn,
				    uint32_t 	baudrate,
				    QueueHandle_t RxQueue,
					osSemaphoreId UsartRxSemaphoreHandle,
					circular_buffer_t* cbuffer
					);
#endif
void mbport_transmit(modbus_port_t* mbport);

#endif
