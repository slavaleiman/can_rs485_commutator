#ifndef _VDEVICE_H_
#define _VDEVICE_H_ 1

// модуль с динамическим распределением адресов modbus
// виртуальные девайс прокси слейва


#define DEVICE_NUM_REGS	128
// если кончаются адреса 

#include "stm32f4xx.h"
typedef struct
{
	uint16_t	data[DEVICE_NUM_REGS][2]; // 128 unnamed regs 0 - reg, 1 - value
	uint16_t 	regs_count;
}device_t;

int8_t device_read_data(device_t* dev, uint16_t reg, uint16_t* value);
int8_t device_write_data(device_t* dev, uint16_t reg, uint16_t value);
uint16_t device_regs_count(device_t* dev);

#endif
