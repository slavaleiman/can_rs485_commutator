// #ifndef __sae1939_71_spn__
// #define __sae1939_71_spn__ 1

// ЭТО СТАНДАРТНЫЕ НОМЕРА SPN из документа SAE1939-71[a]
// но производитель вправе использовать свои адреса

// 16, // Fuel filter (suction side) differential pressure (see also 1382)
// 21, // Engine ecu temperature (use 1136)
// 22, // Extended crankcase blow-by pressure
// 46, // Pneumatic supply pressure
// 51, // Throttle position
// 52, // Engine intercooler temperature
// 53, // Transmission synchronizer clutch value
// 54, // Transmission synchronizer brake value
// 59, // Shift finger gear position
// 60, // Shift finger rail position
// 69, // Two speed axle switch
// 72, // Blower bypass valve position
// 73, // Auxiliary pump pressure
// 74, // Maximum vessel speed limit
// 81, // Particulate trap inlet pressure
// 82, // Air start pressure
// 86, // Cruise control set speed
// 87, // Cruise control high set limit speed
// 88, // Cruise control low set limit speed
// 90, // Power takeoff oil temperature
// 91, // Throttle / Accelerator pedal position 1
// 92, // Percent load at current speed
// 94, // Fuel delivery pressure 35
// 95, // Fuel filter differential pressure

// #endif //__sae1939_71_spn__
