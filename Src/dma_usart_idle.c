/*
    To enable circular buffer, you have to enable IDLE LINE DETECTION interrupt

    __HAL_UART_ENABLE_IT(UART_HandleTypeDef *huart, UART_IT_IDLE);   // enable idle line interrupt
    __HAL_DMA_ENABLE_IT (DMA_HandleTypeDef *hdma, DMA_IT_TC);  // enable DMA Tx cplt interrupt
    also enable RECEIVE DMA
    HAL_UART_Receive_DMA (UART_HandleTypeDef *huart, DMA_RX_Buffer, 255);
    IF you want to transmit the received data uncomment lines

    PUT THE FOLLOWING IN THE MAIN.c

    #define DMA_RX_BUFFER_SIZE          255
    uint8_t DMA_RX_Buffer[DMA_RX_BUFFER_SIZE];

    #define UART_BUFFER_SIZE            256
    uint8_t UART_Buffer[UART_BUFFER_SIZE];
*/

#include "dma_usart_idle.h"
#include "string.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"
#include "cmsis_os.h"
#include "modbus.h"
#include "modbus_port.h"
#include "errors.h"
#include "circular_buffer.h"
#include <stdbool.h>

extern modbus_port_t mbport1;
extern modbus_port_t mbport2;
extern modbus_port_t mbport3;

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart3;
extern UART_HandleTypeDef huart6;

extern circular_buffer_t uart1_cbuffer;
extern circular_buffer_t uart3_cbuffer;
extern circular_buffer_t uart6_cbuffer;

modbus_port_t* get_mbport(UART_HandleTypeDef* huart)
{
    if(huart == &huart1)
        return &mbport1;
    if(huart == &huart3)
        return &mbport2;
    if(huart == &huart6)
        return &mbport3;    
    return NULL;
}

void USART_IRQHandler(UART_HandleTypeDef *huart)
{
    modbus_port_t* port = get_mbport(huart);
    DMA_HandleTypeDef *hdma = huart->hdmarx;

    QueueHandle_t rx_queue              = port->RxQueue;
    osSemaphoreId rx_semaphore_handle   = port->UsartRxSemaphoreHandle;
    if(__HAL_UART_GET_FLAG(huart, UART_FLAG_IDLE))
    {
        (void)huart->Instance->DR;
        __HAL_UART_CLEAR_FLAG(huart, UART_FLAG_IDLE | UART_FLAG_RXNE);
        size_t len = DMA_RX_BUFFER_SIZE - hdma->Instance->NDTR;
        if(len)
        {
#ifdef CHECK_TX
            if(port->is_sending == true) // отправленное сообщение
            {
                port->is_sending = false;
                hdma->Instance->CR &= ~DMA_SxCR_EN;
                modbus_check_trasmition(port, len);
                memset(port->DMA_RX_Buffer, 0, len);
                hdma->Instance->M0AR = (uint32_t)port->DMA_RX_Buffer; /* Set memory address for DMA again */
                hdma->Instance->NDTR = DMA_RX_BUFFER_SIZE;     /* Set number of bytes to receive */
                hdma->Instance->CR |= DMA_SxCR_EN;              /* Start DMA transfer */
            }
            else
#endif
            {
                hdma->Instance->CR &= ~DMA_SxCR_EN;

                message_t mess = (message_t){port->cbuffer->head, len};
                cbuffer_push(port->cbuffer, port->DMA_RX_Buffer, len);
                memset(port->DMA_RX_Buffer, 0, len);

                hdma->Instance->M0AR = (uint32_t)port->DMA_RX_Buffer; /* Set memory address for DMA again */
                hdma->Instance->NDTR = DMA_RX_BUFFER_SIZE;     /* Set number of bytes to receive */
                hdma->Instance->CR |= DMA_SxCR_EN;              /* Start DMA transfer */
                
                BaseType_t xHigherPriorityTaskWoken = pdFALSE;
                xQueueSendToBackFromISR(rx_queue, &mess, &xHigherPriorityTaskWoken);
                xHigherPriorityTaskWoken = pdFALSE;
                xSemaphoreGiveFromISR(rx_semaphore_handle, &xHigherPriorityTaskWoken);
            }
        }
    }
    if(huart->Instance->SR & USART_SR_ORE)
    {
        huart->Instance->SR &= ~USART_SR_ORE;
        (void)huart->Instance->DR;
        huart->Instance->CR1 &= ~USART_CR1_UE;
        huart->Instance->CR3 |= USART_CR3_EIE | USART_CR3_DMAR;
        huart->Instance->CR1 |= USART_CR1_UE;
        // reinit_dma
        memset(port->DMA_RX_Buffer, 0, DMA_RX_BUFFER_SIZE);
        hdma->Instance->CR &= ~DMA_SxCR_EN;
        hdma->Instance->M0AR = (uint32_t)port->DMA_RX_Buffer; /* Set memory address for DMA again */
        hdma->Instance->NDTR = DMA_RX_BUFFER_SIZE;     /* Set number of bytes to receive */
        hdma->Instance->CR |= DMA_SxCR_TCIE | DMA_SxCR_TEIE;
        hdma->Instance->CR |= DMA_SxCR_EN;              /* Start DMA transfer */
        errors_on_error(RS485_OVERRUN_ERROR); // without led
    }
}

void mbport_de_gpio_init(modbus_port_t *port)
{
    // __HAL_RCC_GPIOA_CLK_ENABLE();
    // GPIO_InitTypeDef GPIO_InitStruct = {0};
    // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    // GPIO_InitStruct.Pull = GPIO_NOPULL;
    // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    // GPIO_InitStruct.Pin = GPIO_PIN_15;
    // GPIO_InitStruct.Alternate = GPIO_AF4_USART4;
    // HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}

void dma_usart_idle_init(void)
{
// USART1 TODO init DE pin
// USART3 TODO
// USART6 TODO
    mbport_de_gpio_init(&mbport1);
    mbport_de_gpio_init(&mbport2);
    mbport_de_gpio_init(&mbport3);

    // DE = 1 во время отправки
    // HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, GPIO_PIN_RESET); // DE
    // HAL_GPIO_WritePin(MBPORT2_DE_GPIO_PORT, MBPORT2_DE_GPIO_PIN, GPIO_PIN_RESET);
    // HAL_GPIO_WritePin(MBPORT3_DE_GPIO_PORT, MBPORT3_DE_GPIO_PIN, GPIO_PIN_RESET);

    mbport1.is_sending = false;
    mbport2.is_sending = false;
    mbport3.is_sending = false;
    // USART1
    __HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);   // enable idle line interrupt
    HAL_UART_Receive_DMA(&huart1, mbport1.DMA_RX_Buffer, DMA_RX_BUFFER_SIZE);
    // USART3
    __HAL_UART_ENABLE_IT(&huart3, UART_IT_IDLE);   // enable idle line interrupt
    HAL_UART_Receive_DMA(&huart3, mbport2.DMA_RX_Buffer, DMA_RX_BUFFER_SIZE);
    // USART6
    __HAL_UART_ENABLE_IT(&huart6, UART_IT_IDLE);   // enable idle line interrupt
    HAL_UART_Receive_DMA(&huart6, mbport3.DMA_RX_Buffer, DMA_RX_BUFFER_SIZE);
}

void rs485_recieve_enable(UART_HandleTypeDef* huart)
{
    // HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
    // switch(huart)
    // {
    //     case huart1:
    //         HAL_GPIO_WritePin(HUART1_RS485_EN_GPIO_PORT, HUART1_RS485_EN_GPIO_PIN, GPIO_PIN_RESET);
    //         break;
    //     case huart3:
    //         HAL_GPIO_WritePin(HUART1_RS485_EN_GPIO_PORT, HUART1_RS485_EN_GPIO_PIN, GPIO_PIN_RESET);
    //         break;
    //     case huart4:
    //         HAL_GPIO_WritePin(HUART1_RS485_EN_GPIO_PORT, HUART1_RS485_EN_GPIO_PIN, GPIO_PIN_RESET);
    //         break;
    //     default:
    //         break;
    // }
}

void rs485_transmit_enable(UART_HandleTypeDef* huart)
{
    // HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
    // switch(huart)
    // {
    //     case huart1:
    //         HAL_GPIO_WritePin(HUART1_RS485_EN_GPIO_PORT, HUART1_RS485_EN_GPIO_PIN, GPIO_PIN_SET);
    //         break;
    //     case huart3:
    //         HAL_GPIO_WritePin(HUART1_RS485_EN_GPIO_PORT, HUART1_RS485_EN_GPIO_PIN, GPIO_PIN_SET);
    //         break;
    //     case huart4:
    //         HAL_GPIO_WritePin(HUART1_RS485_EN_GPIO_PORT, HUART1_RS485_EN_GPIO_PIN, GPIO_PIN_SET);
    //         break;
    //     default:
    //         break;
    // }
}

void dma_usart_idle_reinit(UART_HandleTypeDef* huart)
{
    modbus_port_t* port = get_mbport(huart);
    port->is_sending = false;
#ifdef STM32F429xx
    huart->Instance->SR &= ~USART_SR_IDLE;
#elif STM32F746xx
    huart->Instance->ICR |= UART_CLEAR_IDLEF;
#elif STM32F091xx
    huart->Instance->ICR |= UART_CLEAR_IDLEF;
#endif

    memset(port->DMA_RX_Buffer, 0, DMA_RX_BUFFER_SIZE);
    __HAL_UART_ENABLE_IT(huart, UART_IT_IDLE);   // enable idle line interrupt
    HAL_UART_Receive_DMA(huart, port->DMA_RX_Buffer, DMA_RX_BUFFER_SIZE);
}
