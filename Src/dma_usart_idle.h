#ifndef __DMA_USART_IDLE__
#define __DMA_USART_IDLE__ 1
#include "stm32f4xx_hal.h"
#include "main.h"

// extern UART_HandleTypeDef huart1;
// extern UART_HandleTypeDef huart3;
// extern UART_HandleTypeDef huart6;
// #define DMA_HUART_1 &huart1
// #define DMA_HUART_3 &huart3
// #define DMA_HUART_4 &huart6

#define HAL_UART_TRANSMIT(DMA_HUART, BUF,LEN)      \
    rs485_transmit_enable();                \
    HAL_UART_Transmit(DMA_HUART, BUF, LEN, 400);

#define HAL_UART_TRANSMIT_DMA(DMA_HUART, BUF,LEN)      \
    rs485_transmit_enable();                \
    HAL_UART_Transmit_DMA(DMA_HUART, BUF, LEN);

void USART_IRQHandler(UART_HandleTypeDef *huart);

void rs485_recieve_enable(UART_HandleTypeDef* huart);
void rs485_transmit_enable(UART_HandleTypeDef* huart);

void dma_usart_idle_reinit(UART_HandleTypeDef* huart);
#endif
