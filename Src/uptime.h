#ifndef __UPTIME__
#define __UPTIME__ 1
#include "stm32f429xx.h"
uint32_t uptime_update_time(void);
uint32_t uptime(void);
void uptime_string(char* string);

#endif
