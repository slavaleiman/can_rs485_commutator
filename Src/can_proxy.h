#ifndef __CAN_PROXY__
#define __CAN_PROXY__ 1

#include <stdbool.h> 
#include "can_port.h"

typedef struct
{
	// struct
	// {
		// здесь данные для перекладывания
		// uint16_t data;
	// }regs;

    uint32_t update_slave_reg_flags; // for reading slave

    can_port_t* in_port;
    can_port_t* out_port;
}can_proxy_t;

void can_proxy_init(can_port_t* port1, 
                    can_port_t* port2);

void can_proxy_update_output(void);

#endif
