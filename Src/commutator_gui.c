#include "comutator_gui.h"
#include "ugui.h"
#include "ili9341.h"

#define STATUS_WND_MAX_OBJECTS 6
UG_WINDOW window_status;
UG_OBJECT obj_buff_wnd_status[STATUS_WND_MAX_OBJECTS];
UG_OBJECT* object_status;
UG_OBJECT* object_time;
UG_BUTTON button_iostate;
UG_BUTTON button_voltage;
UG_BUTTON button_temperature;
UG_BUTTON button_setup;

static uint8_t shot_gui_fill_frame(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color)
{
    uint16_t w = (x2 - x1 > 0) ? (x2 - x1) : (x1 - x2);
    uint16_t h = (y2 - y1 > 0) ? (y2 - y1) : (y1 - y2);
    if(w == 0) w = 1;
    if(h ==0) h = 1;
    ILI9341_FillRectangle(x1, y1, w, h, color);
    return 0;
}

static uint8_t shot_gui_draw_line(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color)
{
    uint16_t w = (x2 - x1 > 0) ? (x2 - x1) : (x1 - x2);
    uint16_t h = (y2 - y1 > 0) ? (y2 - y1) : (y1 - y2);
    if(w == 0) w = 1;
    if(h ==0) h = 1;
    ILI9341_FillRectangle(x1, y1, w, h, color);
    return 0;
}

static void window_status_callback(UG_MESSAGE* msg)
{
    if (msg->type == MSG_TYPE_OBJECT)
    {
        if ((msg->id    == OBJ_TYPE_BUTTON)
         && (msg->event == OBJ_EVENT_RELEASED))
        {
            switch(msg->sub_id)
            {
                case BTN_ID_0:
                {
                    // show_iostate_window();
                    break;
                }
                case BTN_ID_1:
                {
                    // show_voltage_window();
                    break;
                }
                case BTN_ID_2:
                {
                    // show_temp_window();
                    break;
                }
                case BTN_ID_3:
                {
                    // show_setup_window();
                    break;
                }
                default:
                    break;
            }
        }
    }
}

void shot_gui_status_draw(UG_WINDOW* wnd, UG_OBJECT* obj)
{
    (void)wnd;
    int16_t x = 0;
    int16_t y = 0;
    UG_Object_Coordinates(obj, &x, &y);
    // shot_gui_draw_status(wnd, x, y);
    obj->state &= ~OBJ_STATE_UPDATE;
}

void shot_gui_time_draw(UG_WINDOW* wnd, UG_OBJECT* obj)
{
    (void)wnd;
    char string[64];
    // shot_operation_time(string); // TODO
    int16_t x = 0;
    int16_t y = 0;
    UG_Object_Coordinates(obj, &x, &y);
    UG_PutString(x, y, string);
    obj->state &= ~OBJ_STATE_UPDATE;
}

static void create_status_window(void)
{
    UG_WindowCreate(&window_status, obj_buff_wnd_status, STATUS_WND_MAX_OBJECTS, window_status_callback);
    UG_WindowSetTitleText(&window_status, (char*)"Главное меню");
    UG_WindowSetTitleTextFont(&window_status, &FONT_8X14_RU);

    UG_WindowSetXEnd(&window_status, 320);
    UG_WindowSetYEnd(&window_status, 238);

    uint8_t x = 5;
    uint8_t y = 3;
    const uint8_t btn_width = 53;
    const uint8_t btn_height= 50;

    uint16_t width = UG_WindowGetInnerWidth(&window_status);
    uint16_t height = UG_WindowGetInnerHeight(&window_status);

    UG_ButtonCreate(&window_status, &button_iostate, BTN_ID_0, x, y, x + btn_width, y + btn_height);
    UG_ButtonSetFont(&window_status, BTN_ID_0, &FONT_8X14_RU);
    UG_ButtonSetText(&window_status, BTN_ID_0, (char*)"I/O");
    y += btn_height + 2;
    UG_ButtonCreate(&window_status, &button_voltage, BTN_ID_1, x, y, x + btn_width, y + btn_height);
    UG_ButtonSetFont(&window_status, BTN_ID_1, &FONT_8X14_RU);
    UG_ButtonSetText(&window_status, BTN_ID_1, (char*)"U");
    y += btn_height + 2;
    UG_ButtonCreate(&window_status, &button_temperature, BTN_ID_2, x, y, x + btn_width, y + btn_height);
    UG_ButtonSetFont(&window_status, BTN_ID_2, &FONT_8X14_RU);
    UG_ButtonSetText(&window_status, BTN_ID_2, (char*)"Т");
    y += btn_height + 2;
    UG_ButtonCreate(&window_status, &button_setup, BTN_ID_3, x, y, x + btn_width, y + btn_height);
    UG_ButtonSetFont(&window_status, BTN_ID_3, &FONT_8X14_RU);
    UG_ButtonSetText(&window_status, BTN_ID_3, (char*)"Н");
    y = 20;
    UG_Custom_Object_Create(&window_status, &object_time, OBJ_ID_1, btn_width + 12, y, width - 5, y + 18, shot_gui_time_draw);
    y += 15;
    UG_Custom_Object_Create(&window_status, &object_status, OBJ_ID_0, btn_width + 12, y, width - 5, height - 5, shot_gui_status_draw);
}


static void show_status_window(void)
{
    UG_WindowShow(&window_status);
    // kbd_set_cb_1(&push_button_0);
    // kbd_set_cb_2(&push_button_1);
    // kbd_set_cb_3(&push_button_2);
    // kbd_set_cb_4(&push_button_3);
}

UG_GUI gui;

void commutator_gui_init(void)
{
    UG_Init(&gui, (void(*)(UG_S16, UG_S16, UG_COLOR))ILI9341_DrawPixel, 320, 240);
    /* Register hardware acceleration */
    UG_DriverRegister(DRIVER_DRAW_LINE, shot_gui_draw_line);
    UG_DriverEnable(DRIVER_DRAW_LINE);
    UG_DriverRegister(DRIVER_FILL_FRAME, shot_gui_fill_frame);
    UG_DriverEnable(DRIVER_FILL_FRAME);

	create_status_window();
	show_status_window();
}