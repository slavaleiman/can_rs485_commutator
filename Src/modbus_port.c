#include "modbus_port.h"
#ifndef UNITTEST
#include "modbus_proxy.h"
#include "dma_usart_idle.h"
#include <stdbool.h>
#endif
#include "modbus.h"
#include "errors.h"

void mbport_transmit(modbus_port_t* mbport)
{
#ifndef UNITTEST
	HAL_UART_Transmit_DMA(mbport->huart, mbport->tx_buffer, mbport->tx_size);
    mbport->is_sending = true;
#endif
}

void mbport_process(modbus_port_t* mbport)
{
#ifndef UNITTEST
	osSemaphoreId UsartRxSemaphoreHandle = mbport->UsartRxSemaphoreHandle;
	QueueHandle_t RxQueue = mbport->RxQueue;
	if(xSemaphoreTake(UsartRxSemaphoreHandle, pdMS_TO_TICKS(15)) == pdTRUE)
	{
		message_t mess;
		if(RxQueue)
		{
			if(xQueueReceive(RxQueue, &mess, pdMS_TO_TICKS(15)))
			{
				if(mess.len != 0)
				{
					int ret = mbproxy_on_rtu(mbport, mess.data, mess.len);
					// ответ по таймауту
					if(ret)
					{
						errors_on_error(ret);
					}
					memset(mess.data, 0, mess.len);
				}
			}
		}
	}
#endif	
}

#ifndef UNITTEST
void mbport_init(modbus_port_t* mbport,
					UART_HandleTypeDef *huart,
					IRQn_Type 	IRQn,
				    uint32_t 	baudrate,
				    QueueHandle_t RxQueue,
					osSemaphoreId UsartRxSemaphoreHandle,
					circular_buffer_t* cbuffer
)
{
	mbport->huart = huart;
	mbport->IRQn = IRQn;
    mbport->baudrate = baudrate;
    mbport->RxQueue = RxQueue;
	mbport->UsartRxSemaphoreHandle = UsartRxSemaphoreHandle;
    mbport->is_sending = false;
    mbport->cbuffer = cbuffer;
    if(!mbport->holding_mutex)
        mbport->holding_mutex = xSemaphoreCreateMutex();
}
#endif
