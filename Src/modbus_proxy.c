/*
    PROXY FOR VAR BITRATE BUSSES
*/

#include "main.h"
#include "stm32f4xx.h"
#include "modbus_proxy.h"
#include "dma_usart_idle.h"
#include "modbus.h"
#include "errors.h"
#include "version.h"
#include <stdbool.h>
#include <string.h>
#include "soft_timer.h"
#include "CRC.h"
#include "device.h"

mbproxy_t mbproxy;

enum{
    REQ_PROXY_DATA = 0,
    REQ_SLAVE1_DATA,
    REQ_SLAVE2_DATA
};

// PROXY HAS OWN REGS FOR OPTIONS
int8_t mbproxy_data(uint16_t reg, uint16_t* value)
{
    switch(reg)
    {
        case DEVID_REG:
            *value = DEVICE_ID;
            return 0;
        case SUBID_REG:
            *value = SUB_ID;
            return 0;        
        case RS485_BAUDRATE_1_HREG:
            *value = mbproxy.master->baudrate >> 16;
            return 0;
        case RS485_BAUDRATE_1_LREG:
            *value = mbproxy.master->baudrate & 0xFFFF;
            return 0;

        case RS485_BAUDRATE_2_HREG:
            *value = mbproxy.slave1->baudrate >> 16;
            return 0;
        case RS485_BAUDRATE_2_LREG:
            *value = mbproxy.slave1->baudrate & 0xFFFF;
            return 0;

        case RS485_BAUDRATE_3_HREG:
            *value = mbproxy.slave2->baudrate >> 16;
            return 0;
        case RS485_BAUDRATE_3_LREG:
            *value = mbproxy.slave2->baudrate & 0xFFFF;
            return 0;

        case MODBUS_SLAVE1_TIMEOUT:
            *value = mbproxy.slave1->timeout;
            return 0;
        case MODBUS_SLAVE2_TIMEOUT:
            *value = mbproxy.slave2->timeout;
            return 0;

        default:
            break;
    }
    return DATA_ADDR_ERR;
}

int8_t mbproxy_read_data(uint16_t reg, uint16_t* value)
{
    switch(mbproxy.requested_data)
    {
        case REQ_PROXY_DATA:
            return mbproxy_data(reg, value);
        case REQ_SLAVE1_DATA:
            return device_read_data(mbproxy.slave1->device, reg, value);
        case REQ_SLAVE2_DATA:
            return device_read_data(mbproxy.slave2->device, reg, value);
        default:
            return -1;
    }
}

int8_t mbproxy_write(uint16_t reg, uint16_t value)
{
    uint16_t update_ext_mem = 0;
    switch(reg)
    {
        case RS485_BAUDRATE_1_HREG:
            mbproxy.master->baudrate &= ~(0xFFFF << 16);
            mbproxy.master->baudrate |= value << 16;
            return 0;
        case RS485_BAUDRATE_1_LREG:
            mbproxy.master->baudrate &= ~0xFFFF;
            mbproxy.master->baudrate |= value;
            update_ext_mem |= (1 << 8);
            break;
        case RS485_BAUDRATE_2_HREG:
            mbproxy.slave1->baudrate &= ~(0xFFFF << 16);
            mbproxy.slave1->baudrate |= value << 16;
            return 0;
        case RS485_BAUDRATE_2_LREG:
            mbproxy.slave1->baudrate &= ~0xFFFF;
            mbproxy.slave1->baudrate |= value;
            update_ext_mem |= (1 << 8);
            break;
        case RS485_BAUDRATE_3_HREG:
            mbproxy.slave2->baudrate &= ~(0xFFFF << 16);
            mbproxy.slave2->baudrate |= value << 16;
            return 0;
        case RS485_BAUDRATE_3_LREG:
            mbproxy.slave2->baudrate &= ~0xFFFF;
            mbproxy.slave2->baudrate |= value;
            update_ext_mem |= (1 << 8);
            break;

        case MODBUS_SLAVE1_TIMEOUT:
            mbproxy.slave1->timeout = value;
            return 0;
        case MODBUS_SLAVE2_TIMEOUT:
            mbproxy.slave2->timeout = value;
            return 0;            

        default:
            return DATA_ADDR_ERR;
    }
    if(update_ext_mem)
    {
        // int8_t ret = ext_mem_store();
        // if(ret)
        // {
        //     ERROR(EXT_MEM_WRITE_ERROR);
        //     return FATAL_ERR;
        // }
    }
    return 0;
}

int8_t mbproxy_write_data(uint16_t reg, uint16_t value)
{
    switch(mbproxy.requested_data)
    {
        case REQ_PROXY_DATA:
            return mbproxy_write(reg, value);
        case REQ_SLAVE1_DATA:
            return device_write_data(mbproxy.slave1->device, reg, value);
        case REQ_SLAVE2_DATA:
            return device_write_data(mbproxy.slave2->device, reg, value);
        default:
            return -1;
    }
}

void mbproxy_inc_crc_err_rq(void)
{
    ++mbproxy.err_crc_rq;
}

void mbproxy_inc_handled_rq(void)
{
    ++mbproxy.handled_rq;
}

void mbproxy_inc_total_rq(void)
{
    ++mbproxy.total_rq;
}

void mbproxy_inc_send_err(void)
{
    ++mbproxy.err_send;
}

void mbproxy_process(void)
{
    // test state - change values
}

int8_t mbproxy_rs485_port_init(modbus_port_t* port)
{
    if(!port->baudrate)
        return -1;
    HAL_NVIC_DisableIRQ(port->IRQn);
    HAL_UART_DeInit(port->huart);
    port->huart->Init.BaudRate = port->baudrate;
    port->huart->Init.WordLength = UART_WORDLENGTH_8B;
    port->huart->Init.StopBits = UART_STOPBITS_1;
    port->huart->Init.Parity = UART_PARITY_NONE;
    port->huart->Init.Mode = UART_MODE_TX_RX;
    port->huart->Init.HwFlowCtl = UART_HWCONTROL_NONE;
    port->huart->Init.OverSampling = UART_OVERSAMPLING_16;

    if(HAL_UART_Init(port->huart) != HAL_OK)
    {
        return -1;
    }
    HAL_NVIC_EnableIRQ(port->IRQn);
    dma_usart_idle_reinit(port->huart);
    return 0;
}

void mbproxy_init(  modbus_port_t* master, 
                    modbus_port_t* slave1,
                    modbus_port_t* slave2,
                    device_t* device1,
                    device_t* device2
                    )
{
    mbproxy.master = master; // master
    mbproxy.slave1 = slave1; // device1
    mbproxy.slave2 = slave2; // device2
    mbproxy.slave1->device = device1;
    mbproxy.slave2->device = device2;
    mbproxy_rs485_port_init(mbproxy.master);
    mbproxy_rs485_port_init(mbproxy.slave1);
    mbproxy_rs485_port_init(mbproxy.slave2);
}

void on_slave1_timeout(void)
{
    soft_timer_stop(MBSLAVE1_TIMER_INDEX);
    modbus_port_t* mbport = mbproxy.master;

    // здесь в mbport->tx_buffer должен лежать обработанный ответ
    // TODO проверить что ответ готов 
    mbport_transmit(mbport);
}

void on_slave2_timeout(void)
{
    soft_timer_stop(MBSLAVE2_TIMER_INDEX);
    modbus_port_t* mbport = mbproxy.master;

    // здесь в mbport->tx_buffer должен лежать обработанный ответ
    // TODO проверить что ответ готов
    mbport_transmit(mbport);
}

// send to slave
void send_port_request(modbus_port_t *mbport, uint8_t *data, uint8_t len)
{
    for(uint8_t i = 0; i < len; ++i)
        mbport->tx_buffer[i] = data[i];
    mbport->tx_size = len;
    mbport_transmit(mbport);
}

void on_commutator_request(modbus_port_t * port)
{
    if(!modbus_on_rtu(port, port->rx_buffer, port->rx_size))
        mbport_transmit(port);
}

void on_port_request(modbus_port_t *master_port, modbus_port_t *output_port)
{
    // посылаем запрос параметра в блок и запускаем софт таймер
    // если ответ не успел прийти - отправляем текущее значение 
    // если пришел - отправляем ответ
 
    send_port_request(output_port, master_port->rx_buffer, master_port->rx_size);
    soft_timer_init(MBSLAVE1_TIMEOUT, on_slave1_timeout, 1, MBSLAVE1_TIMER_INDEX);
    
    // обрабатываем запрос на случай таймаута
    // но сразу ответ не посылаем
    // сразу записать, а потом если будет ответ нормальный  - переписать
    //                      если ответ отрицательный,       - то стереть значение в регистре           
    modbus_on_rtu(master_port, master_port->rx_buffer, master_port->rx_size);
    // ответ надо положить в device->tx_buffer 
}

int8_t response_f3(uint8_t* message, uint8_t len)
{
    // если это ответ на запрос на чтение
    // сохраняем в девайс новое значение -> 
    // если он с ошибкой - стираем значение в девайсе, но мы не знаем номер регистра

    // uint8_t errcode = response_holding_reg(message, len);
    // if(errcode)
    //     response_response_error(message, errcode);
    // else
    //     response_inc_handled_rq();

    return 0;
}

int8_t response_f6(uint8_t* message, uint8_t len)
{
    // 
    return 0;
}

int8_t response_f16(uint8_t* message, uint8_t len)
{
    // если это ответ это запрос на запись то при поступлении подтверждения
    // тоже обновляем регистр в прокси, запрос на запись должен быть уже обработан
    return 0;
}

int8_t on_slave_response(modbus_port_t *slave_port)
{
// если это ответ на запрос на чтение, сохраняем данные в регистр 
// обновляем значение в прокси
// если это ответ это запрос на запись то при поступлении подтверждения
// тоже обновляем регистр в прокси
// если произошла ошибка то, данные не записываем в прокси регистр
    uint8_t *message = slave_port->rx_buffer;
    uint8_t len = slave_port->rx_size;
    const uint8_t func_num = slave_port->rx_buffer[1];
    if(!(func_num & 0x80))
    {
        switch(func_num)
        {
            case 3: // ответ на чтение значений из нескольких регистров хранения (Read Holding Registers).
                return response_f3(message, len);
            case 6: // ответ на запись значения в один регистр хранения
                return response_f6(message, len);
            case 16: // ответ на запись значений в несколько регистров хранения (Preset Multiple Registers)
                return response_f16(message, len);
            default:
                break;
        }
    }else{
        // при ошибке - стираем регистр
        // если ошибка
        // ответ на запрос обработанный device'ом лежит в device->tx_buffer
    }
    // parse_modbus
    // int8_t ret = mbport_update_reg(mbport->rx_buffer, len);
    return 0;
}

int8_t on_slave_late_response(modbus_port_t *slave_port)
{
    return 0;
}

int8_t master_porton_rtu(modbus_port_t *mbport)
{
    // если запрос от мастера
    uint8_t addr = mbport->rx_buffer[0];
    switch(addr)
    {
        // если адресован блок-м
        case BLOCKM_CABIN1_ADDR:
        case BLOCKM_CABIN2_ADDR:
            mbproxy.requested_data = REQ_SLAVE1_DATA;
            on_port_request(mbport, mbproxy.slave1);
            break;
        case SADKO_MODBUS_ADDR:
            mbproxy.requested_data = REQ_SLAVE1_DATA;
            on_port_request(mbport, mbproxy.slave2);
            break;
        case COMMUTATOR_MODBUS_ADDR:
            mbproxy.requested_data = REQ_PROXY_DATA;
            on_commutator_request(mbport);
            break;
        default:
            mbproxy.requested_data = -1;
            break;
    }
    return 0;
}

int8_t slave_port1_on_rtu(modbus_port_t* slave_port)
{
    int8_t ret = 0;
    if(soft_timer_is_active(MBSLAVE1_TIMER_INDEX))
    {
        soft_timer_stop(MBSLAVE1_TIMER_INDEX);
        // copy to master tx buffer
        mbproxy.master->tx_size = slave_port->rx_size;
        memcpy(mbproxy.master->tx_buffer, slave_port->rx_buffer, slave_port->rx_size);
        mbport_transmit(mbproxy.master);
    }
    on_slave_response(slave_port);
    return ret;
}

int8_t slave_port2_on_rtu(modbus_port_t* slave_port)
{
    if(soft_timer_is_active(MBSLAVE2_TIMER_INDEX))
    {
        soft_timer_stop(MBSLAVE2_TIMER_INDEX);
        mbproxy.master->tx_size = slave_port->rx_size;
        memcpy(mbproxy.master->tx_buffer, slave_port->rx_buffer, slave_port->rx_size);
        mbport_transmit(mbproxy.master);
    }
    on_slave_response(slave_port);
    return 0;
}

int mbproxy_on_rtu(modbus_port_t *mbport, uint8_t *data_start, uint8_t len)
{
    int ret = 0;
    if(mbport->cbuffer->tail != data_start)
        return DATA_RECEIVE_ERROR;
    if(cbuffer_pop(mbport->cbuffer, (uint8_t*)&mbport->rx_buffer, len) != 0)
        return CBUFFER_POP_ERROR;

    mbport->rx_size = len;
    if(mbport == mbproxy.master)
    {
        ret = master_porton_rtu(mbport);
        return ret;
    }
    // если пришел ответ от блок-м 
    else if(mbport == mbproxy.slave1)
    {
        ret = slave_port1_on_rtu(mbport);
        return ret;
    }
    // ответ от садко
    else if(mbport == mbproxy.slave2)
    {
        ret = slave_port2_on_rtu(mbport);
        return ret;
    }
    return 0; // no responce now
}

uint16_t mbproxy_module_id(void)
{
    return DEVICE_ID;
}

uint16_t mbproxy_status(void)
{
    return mbproxy.update_slave_reg_flags;
}
