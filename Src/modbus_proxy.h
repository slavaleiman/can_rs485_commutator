#ifndef __MBPROXY__
#define __MBPROXY__ 1

#include <stdbool.h>
#include "modbus_port.h"
#include "soft_timer.h"

#define TORBR_TORQUE 0xA5A5
#define TORBR_BREAK	 0x5A5A

#define DEVICE_ID 	0xF006
#define SUB_ID 		0x0013

#define MBSLAVE1_TIMER_INDEX 	0
#define MBSLAVE2_TIMER_INDEX	1

#define BLOCKM_CABIN1_ADDR		30
#define BLOCKM_CABIN2_ADDR 		29
#define SADKO_MODBUS_ADDR		31

#define MBSLAVE1_TIMEOUT 		10 // ms
#define MBSLAVE2_TIMEOUT 		10

#define COMMUTATOR_MODBUS_ADDR  34

enum{
	DEVID_REG 		= 0, // Идентификатор типа устройства	0xF003
	SUBID_REG 		= 1, // Идентификатор подтипа устройства	0x0000

    RS485_BAUDRATE_1_HREG = 8037,
    RS485_BAUDRATE_1_LREG = 8038,

    RS485_BAUDRATE_2_HREG = 8039,
    RS485_BAUDRATE_2_LREG = 8040,

    RS485_BAUDRATE_3_HREG = 8041,
    RS485_BAUDRATE_3_LREG = 8042,

    MODBUS_SLAVE1_TIMEOUT,
    MODBUS_SLAVE2_TIMEOUT
}mbproxy_regs;

typedef struct
{
    uint16_t program_crc; // for self check
    uint16_t ext_mem_crc;
    uint32_t change_time;
    uint32_t total_rq;   // счетчик запросов
    uint32_t handled_rq; // счетчик обработанных запросов
    uint32_t err_crc_rq; // счетчик запросов с ошибкой CRC
    uint32_t err_param_rq; // счетчик запросов с некорректными параметрами
    uint32_t err_send;  // счетчик ошибок при отправке

    uint32_t update_slave_reg_flags; // for reading slave

    modbus_port_t* master;
    modbus_port_t* slave1;
    modbus_port_t* slave2;

    soft_timer_t slave1_timer;
    soft_timer_t slave2_timer;

    int8_t requested_data;
    // uint8_t tx_buffer[PORT_BUFFER_SIZE];
    // uint8_t tx_size;
}mbproxy_t;

void mbproxy_init(  modbus_port_t* port1, 
                    modbus_port_t* port2,
                    modbus_port_t* port3,
                    device_t* device1,
                    device_t* device2
                    );

int8_t 	mbproxy_write_reg(uint16_t reg, uint16_t value);
void 	mbproxy_inc_crc_err_rq(void);
void 	mbproxy_inc_handled_rq(void);
void 	mbproxy_inc_total_rq(void);
void 	mbproxy_inc_send_err(void);
uint32_t operation_time(mbproxy_t mbproxy);
int 	mbproxy_on_rtu(modbus_port_t *mbport, uint8_t *data, uint8_t len);
int8_t mbproxy_response_error(uint8_t* message, uint8_t error_code);
int8_t mbproxy_response_echo(uint8_t* message, uint8_t len);
int8_t mbproxy_read_data(uint16_t reg, uint16_t* value);
int8_t mbproxy_write_data(uint16_t reg, uint16_t value);

#endif
