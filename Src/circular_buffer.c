#include <string.h>
#include "circular_buffer.h"
#include <stdint.h>

int cbuffer_push(circular_buffer_t *cbuff, uint8_t *item, size_t size)
{
    if(cbuff->count + size > cbuff->maxlen)
    {
        return -1;
    }
    
    if(cbuff->head + size < cbuff->buffer_end)
        memcpy(cbuff->head, item, size);
    else{
        int skip = cbuff->buffer_end - cbuff->head;
        memcpy(cbuff->head, item, skip);
        memcpy(cbuff->buffer, &item[skip], size - skip);
    }

    cbuff->head = (char*)cbuff->head + size;
    if(cbuff->head >= cbuff->buffer_end)
        cbuff->head -= cbuff->maxlen;

    cbuff->count += size;
    return 0;
}

int cbuffer_pop(circular_buffer_t *cbuff, uint8_t *item, size_t size)
{
    if(cbuff->count - size < 0)
    {
        return -1;
    }

    uint32_t skip = 0;
    if(cbuff->tail + size <= cbuff->buffer_end)
    {
        memcpy(item, cbuff->tail, size);
    }else{
        skip = cbuff->buffer_end - cbuff->tail;
        memcpy(item, cbuff->tail, skip);
        memcpy(&item[skip], cbuff->buffer, size - skip);
    }

    cbuff->tail = (char*)cbuff->tail + size;
    if(cbuff->tail >= cbuff->buffer_end)
        cbuff->tail -= cbuff->maxlen;
    cbuff->count -= size;
    return 0;
}
