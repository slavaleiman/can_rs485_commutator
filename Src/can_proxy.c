
#include "can_proxy.h"

can_proxy_t can_proxy;

int8_t can_proxy_port_init(can_port_t* port)
{
    // NOW INITIALIZED IN main.c by MX_CAN1_Init() + .._hal_msp.c file 
    return 0;
}

void can_proxy_init(can_port_t* in_port, 
                    can_port_t* out_port)
{
    can_proxy.in_port = in_port;
    can_proxy.out_port = out_port;
    can_proxy_port_init(in_port);
    can_proxy_port_init(out_port);
}
