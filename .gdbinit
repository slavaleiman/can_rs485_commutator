# add-auto-load-safe-path C:\work\PLC_TECHO\PLC_TECHNO_F0\.gdbinit
tar ext :4242
set print pretty on
set pagination off

define killtims
    set TIM6->CR1 &= ~1
    set TIM1->CR1 &= ~1
    set TIM2->CR1 &= ~1
    set TIM3->CR1 &= ~1
    set TIM7->CR1 &= ~1
end

define killall
    set TIM6->CR1 &= ~1
    set TIM1->CR1 &= ~1
    set TIM2->CR1 &= ~1
    set TIM3->CR1 &= ~1
    set TIM7->CR1 &= ~1
    set USART4->CR1 &= ~1
end

define resumeall
    set TIM6->CR1 |= 1
    set TIM1->CR1 |= 1
    set TIM2->CR1 |= 1
    set TIM3->CR1 |= 1
    set TIM7->CR1 |= 1
    set USART4->CR1 |= 1
end

define t1
    p/t *TIM1
end

define t2
    p/t *TIM2
end

define setall
    set device.input[0].mode = $arg0
    set device.input[1].mode = $arg0
    set device.input[2].mode = $arg0
    set device.input[3].mode = $arg0
    set device.input[4].mode = $arg0
    set device.input[5].mode = $arg0
    set device.input[6].mode = $arg0
    set device.input[7].mode = $arg0
end

define water_marks
    p lcdHighWaterMark
    p inputHighWaterMark
    p tickHighWaterMark
    p adcHighWaterMark
end

define puart
    p/t *USART1
    p/t *USART3
end

define pc
    p/t CAN1.ESR
    p/x CAN1.sFIFOMailBox
end 

b HardFault_Handler
#b main
#b USART_IRQHandler
#b modbus_on_rtu
#b CAN1_RX0_IRQHandler
b mbproxy_port_on_rtu
c
