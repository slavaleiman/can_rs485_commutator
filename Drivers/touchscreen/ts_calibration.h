#ifndef __TS_CALIB__
#define __TS_CALIB__ 1

#include "stdbool.h"

bool ts_get_position(uint16_t* x, uint16_t* y);
void ts_default_calibration(void);
void ts_init(void);

#endif //__TS_CALIB__
